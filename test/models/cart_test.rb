require 'test_helper'

class CartTest < ActiveSupport::TestCase
  test "should add a product to the cart" do
    cart = Cart.create
    product = products(:ruby)
    cart.add_product(product)
    cart.save!
    assert_equal 1, cart.line_items.count
    assert_equal product.price, cart.line_items[0].price
    assert_equal 1, cart.line_items[0].quantity

    cart.add_product(products(:one))
    cart.save!
    assert_equal 2, cart.line_items.count

    assert_equal 59.49, cart.total_price.to_f
  end

  test "should increment quantity" do
    cart = Cart.create
    product = products(:ruby)
    cart.add_product(product)
    cart.save!
    assert_equal 1, cart.line_items.count
    ci = cart.add_product(product)
    ci.save!
    cart.save!
    assert_equal 1, cart.line_items.count
    assert_equal product.price, cart.line_items[0].price
    assert_equal 2, cart.line_items[0].quantity
    assert_equal 99.0, cart.total_price.to_f
  end
end
