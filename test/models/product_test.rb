require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  fixtures :products

  test "product attributes must not be empty" do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:price].any?
    assert product.errors[:image_url].any?
  end

  def new_product overwrite={}
    defaults = {
      title: "My Book Title",
      description: "yyy",
      price: 1,
      image_url: "zzz.jpg"
    }
    Product.new(defaults.merge(overwrite))
  end

  test "product price must be positive" do
    product = new_product
    product.price = -1
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"],
      product.errors[:price]

    product.price = -1
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"],
      product.errors[:price]

    product.price = 1
    assert product.valid?
  end

  test "image url" do
    ok = %w{ fred.gif fred.jpg fred.png Fred.JPG Gred.Jpg
             http://a.b.c/x/y/z/fred.gif }
    bad = %w{ fred.doc fred.gif/more fred.gif.more }

    ok.each do |name|
      assert new_product(image_url: name).valid?, "#{name} should be valid"
    end

    bad.each do |name|
      assert new_product(image_url: name).invalid?, "#{name} shouldn't be valid"
    end
  end

  test "product is not valid without a unique title" do
    # products(:ruby) accesses the 'ruby' fixture in products.yaml
    product = new_product(title: products(:ruby).title)
    assert product.invalid?
    assert_equal ["has already been taken"], product.errors[:title]
    # The following line tests the same as the previous.
    assert_equal [I18n.translate('errors.messages.taken')],
                 product.errors[:title]
  end

  test "title should be longer than 9 characters" do
    product = new_product title: "short"
    assert product.invalid?
    assert_equal ["must be longer than 9 characters"],
                 product.errors[:title]

    assert new_product(title: "a long title").valid?
  end

end
