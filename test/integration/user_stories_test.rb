require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  fixtures :products

  # A user goes to the store index page. They select a product, adding it to
  # their cart. They then check out, filling in their details on the checkout
  # form. When they submit, an order is created in the database containing
  # their information, along with a single line item corresponding to the
  # product they added to their cart. Once the order has been received, an
  # email is sent confirming their purchase.
  test "buying a product" do
    LineItem.delete_all
    Order.delete_all
    ruby_book = products(:ruby)

    # A user goes to the store index page.
    get "/"
    assert_response :success
    assert_template "index"

    # They select a product, adding it to their cart.
    xml_http_request :post, '/line_items', product_id: ruby_book.id
    assert_response :success
    cart = Cart.find(session[:cart_id])
    assert_equal 1, cart.line_items.size
    assert_equal ruby_book, cart.line_items[0].product

    # The then check out ...
    get "/orders/new"
    assert_response :success
    assert_template "new"

    post_via_redirect "/orders",
                      order: {
                        name: "Luke Skywalker",
                        address: "Here and There",
                        email: "luke@skywalker.com",
                        pay_type: "Check"
                      }
    assert_response :success
    assert_template "index"
    cart = Cart.find(session[:cart_id])
    assert_equal 0, cart.line_items.size

    orders = Order.all
    assert_equal 1, orders.size
    order = orders[0]

    assert_equal "Luke Skywalker", order.name
    assert_equal "Here and There", order.address
    assert_equal "luke@skywalker.com", order.email
    assert_equal "Check", order.pay_type

    assert_equal 1, order.line_items.size
    line_item = order.line_items[0]
    assert_equal ruby_book, line_item.product

    mail = ActionMailer::Base.deliveries.last
    assert_equal ["luke@skywalker.com"], mail.to
    assert_equal "from@example.com", mail[:from].value
    assert_equal "Pragmatic Store Order Confirmation", mail.subject
  end
end
