class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy
  PYMENT_TYPES = ['Check', 'Credit card', 'Purchase order']

  validates :name, :address, :email, presence: true
  validates :pay_type, inclusion: PYMENT_TYPES

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      # The following line prevents the item to be destroyed after the cart is
      # destroyed.
      item.cart_id = nil
      line_items << item
    end
  end
end
